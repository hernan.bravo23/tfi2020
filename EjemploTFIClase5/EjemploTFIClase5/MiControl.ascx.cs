﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
public partial class MiControl : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    

    public string Etiqueta
    {
        get { return Label1.Text; }
        set { Label1.Text= value; }
    }

    public bool Validar()
    {
        txt.BackColor = (string.IsNullOrWhiteSpace(txt.Text)) ? Color.Coral : Color.White;
        return !string.IsNullOrWhiteSpace(txt.Text);
    }
}