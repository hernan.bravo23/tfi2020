﻿
Partial Class _Default
    Inherits System.Web.UI.Page

    Private Sub _Default_Load(sender As Object, e As EventArgs) Handles Me.Load

    End Sub
    Protected Sub GridView1_DataBound(sender As Object, e As EventArgs) Handles GridView1.DataBound

        Dim ls As List(Of Registro) = Session("reg")

        For Each reg As GridViewRow In GridView1.Rows
            Dim txt As New TextBox() With {.ClientIDMode = ClientIDMode.Static, .ID = "txtGrilla" & reg.Cells(0).Text, .EnableViewState = True}
            txt.Attributes.Add("runat", "server")
            txt.Text = ((From r As Registro In ls
                         Where r.Codigo = Integer.Parse(reg.Cells(0).Text)
                         Select r).First).Nombre
            reg.Cells(1).Controls.Add(txt)

        Next
    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            Dim registros As New List(Of Registro)
            For n = 0 To 10
                registros.Add(New Registro)

            Next
            Session("reg") = registros
            GridView1.DataSource = registros
            GridView1.DataBind()

        End If
    End Sub
    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim ls As List(Of Registro) = Session("reg")
        Dim elementos = Request.Form.AllKeys.Where(Function(x) x.Contains("txtGrilla")).ToList()
        ' MsgBox(Request.Form.AllKeys)
        For Each elemento In elementos

            Dim st As String = Request.Form(elemento)
            Dim id As String = elemento.Substring(elemento.IndexOf("txtGrilla") + 9)
            Dim p As Registro = (From reg As Registro In ls
                                 Where reg.Codigo = Integer.Parse(id)
                                 Select reg).First
            p.Nombre = st

        Next
        GridView1.DataSource = Nothing
        GridView1.DataSource = ls
        GridView1.DataBind()
    End Sub
End Class
