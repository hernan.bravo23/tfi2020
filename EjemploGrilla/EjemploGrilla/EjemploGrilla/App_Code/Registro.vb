﻿Imports Microsoft.VisualBasic

Public Class Registro
    Private Shared i As Integer
    Public Sub New()
        i += 1
        Me.id = i
    End Sub

    Private id As Integer
    Public ReadOnly Property Codigo() As Integer
        Get
            Return id
        End Get

    End Property

    Private _nombre As String
    Public Property Nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property

End Class
