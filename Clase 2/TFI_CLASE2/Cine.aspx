﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Cine.aspx.cs" Inherits="Cine" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
      <div>
            <asp:Button ID="btnNuevo" runat="server" Text="Nuevo" OnClick="btnNuevo_Click" />
            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/DEfault.aspx">IR A GENEROS</asp:HyperLink>
            <br /><br />

            <asp:HiddenField ID="hID" runat="server" Value ="0" /><br />
            <asp:TextBox ID="txtNombre" Placeholder="Ingrese la pelicula" runat="server"></asp:TextBox>
            <br /><br />
          <asp:DropDownList ID="cbGenero" runat="server"></asp:DropDownList>

            <br /><br />

            <asp:Button ID="btnGuardar" runat="server" Text="Guardar" OnClick="btnGuardar_Click" />
            <br /><br />

            <asp:GridView ID="Grilla" runat="server" OnRowCommand="Grilla_RowCommand">
                <Columns>
                    <asp:ButtonField CommandName="btnSeleccionar" Text="Seleccionar" />
                    <asp:ButtonField CommandName="btnBorrar" Text="Borrar" />
                </Columns>
            </asp:GridView>

        </div>
    </form>
</body>
</html>
