﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class MP_GENERO : MAPPER<BE.GENERO>
    {
        public MP_GENERO()
        {
            acceso = new ACCESO();
            accesopropio = true;
        }

        internal MP_GENERO(ACCESO ac)
        {
            acceso = ac;
            accesopropio = false;
        }

        public override int Borrar(GENERO objeto)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add( acceso.CrearParametro("@id", objeto.Id));
            Abrir();

            acceso.IniciarTransaccion();

            MP_PELICULA mp = new MP_PELICULA(acceso);

            List<BE.PELICULA> pelis = (from p in mp.Listar()
                                       where p.Genero.Id == objeto.Id
                                       select p).ToList();

            int cantidad = pelis.Count;
            foreach (BE.PELICULA p in pelis)
            {
                if (mp.Borrar(p) == 1)
                {
                    cantidad -= 1;
                }
            }
            int resultado;
            if (cantidad == 0)
            {
                resultado = acceso.Escribir("GENERO_BORRAR", parametros);
            }
            else
            {
                resultado = -1;
            }

            if (resultado >= 0)
            {
                acceso.ConfirmarTransaccion();
            }
            else
            {
                acceso.DeshacerTransaccion();
            }

            Cerrar();
            return resultado;
        }

        public override int Editar(GENERO objeto)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("@id", objeto.Id));
            parametros.Add(acceso.CrearParametro("@nom", objeto.Nombre));
            Abrir();
            int resultado = acceso.Escribir("GENERO_EDITAR", parametros);
            Cerrar();
            return resultado;
        }

        public override int Insertar(GENERO objeto)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("@nom", objeto.Nombre));
            Abrir();
            int resultado = acceso.Escribir("GENERO_INSERTAR", parametros);
            Cerrar();
            return resultado;
        }

        public override List<GENERO> Listar()
        {
            Abrir();
            DataTable tabla = acceso.Leer("GENERO_LISTAR");
            Cerrar();
            List<GENERO> generos = new List<GENERO>();
            foreach (DataRow registro in tabla.Rows)
            {
                generos.Add(new GENERO(int.Parse(registro["ID_GENERO"].ToString()), registro["GENERO"].ToString()));
            }
            return generos;


        }
    }
}
