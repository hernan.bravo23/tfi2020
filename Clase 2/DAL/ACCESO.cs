﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;


namespace DAL
{
    internal class ACCESO
    {                    

        SqlConnection conexion;
        
        SqlTransaction transaccion;

        public void Abrir()
        {
            conexion = new SqlConnection();
            conexion.ConnectionString = "Initial Catalog=CINE; Data Source=.\\SQLEXPRESS; Integrated Security=SSPI";
            conexion.Open();
        }

        public void Cerrar()
        {
            conexion.Close();
            conexion.Dispose();
            conexion = null;
            GC.Collect();
        }

        public void IniciarTransaccion()
        {
            transaccion= conexion.BeginTransaction();
            
        }
        
        public void ConfirmarTransaccion()
        {
            transaccion.Commit();
            
        }

        public void DeshacerTransaccion()
        {
            transaccion.Rollback();
        }

        private SqlCommand CrearComando(string nombre, List<SqlParameter> parametros = null)
        {
            SqlCommand comando = new SqlCommand();
            comando.CommandText = nombre;
            comando.CommandType = CommandType.StoredProcedure;
            comando.Connection = conexion;
            if (transaccion != null)
            {
                comando.Transaction = transaccion;
            }


            if (parametros != null && parametros.Count > 0)
            {
                comando.Parameters.AddRange(parametros.ToArray());
            }
            return comando;            
        }

        public int Escribir(string nombre, List<SqlParameter> parameters)
        {
            SqlCommand comando = CrearComando(nombre, parameters);
            int filasafectadas;
            try
            {
                filasafectadas = comando.ExecuteNonQuery();
            }
            catch
            {
                filasafectadas = -1;
            }
            comando.Parameters.Clear();
            comando.Dispose();
            comando = null;
            GC.Collect();
            return filasafectadas;  
        }

        public DataTable Leer(string nombre, List<SqlParameter> parameters = null)
        {
            SqlDataAdapter adaptador = new SqlDataAdapter();
            adaptador.SelectCommand = CrearComando(nombre, parameters);
            DataTable tabla = new DataTable();

            adaptador.Fill(tabla);

            return tabla;
        }

        public SqlParameter CrearParametro(string nombre, string valor)
        {
            SqlParameter p = new SqlParameter();
            p.ParameterName = nombre;
            p.Value = valor;
            p.SqlDbType = SqlDbType.Text;
            return p;
        }

        public SqlParameter CrearParametro(string nombre, int valor)
        {
            SqlParameter p = new SqlParameter();
            p.ParameterName = nombre;
            p.Value = valor;
            p.SqlDbType = SqlDbType.Int;
            return p;
        }


    }
}
