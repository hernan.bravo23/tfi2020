﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;

namespace BLL
{
    public class GESTOR_PELICULA : GESTOR<BE.PELICULA>
    {
        private DAL.MP_PELICULA mp = new DAL.MP_PELICULA();

        public override int Borrar(PELICULA objeto)
        {
            return mp.Borrar(objeto);
        }

        public override int Grabar(PELICULA objeto)
        {
            int resultado;
            if (objeto.Id == 0)
            {
                resultado = mp.Insertar(objeto);
            }
            else
            {
                resultado = mp.Editar(objeto);
            }


            return resultado;
        }

        public override List<PELICULA> Listar()
        {
           return mp.Listar();
        }

        public override PELICULA Listar(int id)
        {
            return mp.Listar(id);
        }
    }
}
