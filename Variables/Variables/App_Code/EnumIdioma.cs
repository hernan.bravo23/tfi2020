﻿using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;

public enum EnumIdioma
{
    Español = 0,
    Ingles = 1,
    Ruso = 2
}