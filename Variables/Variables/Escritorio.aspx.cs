﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Escritorio : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["usuario"] == null)
        {
            Response.Redirect("Default.aspx");

        }
        else
        {
            IDIOMA i = (IDIOMA)Session["IDIOMA"];

            Label1.Text = Session["Usuario"].ToString() + " " + Session["Contraseña"].ToString();
            Label2.Text = i.Idioma + " " +  Session.SessionID;


            GridView1.DataSource = Session;
            GridView1.DataBind();
        }


    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        Response.Redirect("Default.aspx");
        
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        IDIOMA i = (IDIOMA)Session["IDIOMA"];
        i.Idioma = i.Idioma.ToUpper();


        Label1.Text = Session["Usuario"].ToString() + " " + Session["Contraseña"].ToString();
        Label2.Text = i.Idioma + " " + Session.SessionID;


        GridView1.DataSource = Session;
        GridView1.DataBind();
    }

    protected void Button3_Click(object sender, EventArgs e)
    {
        IDIOMA i = new IDIOMA();
        i.Idioma = TextBox1.Text;

        List<IDIOMA> idiomas = (List<IDIOMA>)Application["idiomas"];

        idiomas.Add(i);

    }
}