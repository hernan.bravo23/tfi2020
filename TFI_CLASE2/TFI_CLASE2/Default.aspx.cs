﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    BLL.GESTOR_GENERO gestor = new BLL.GESTOR_GENERO();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            Enlazar();
        }
    }

    void Enlazar()
    {
        Grilla.DataSource = gestor.Listar();
        Grilla.DataBind();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        txtNombre.Text = "";
        hID.Value = "0";
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(txtNombre.Text))
        {
            BE.GENERO genero = new BE.GENERO();

            genero.Id = int.Parse(hID.Value);
            genero.Nombre = txtNombre.Text;

            gestor.Grabar(genero);
            Enlazar();
            txtNombre.Text = "";
            hID.Value = "0";
        }
    }

    protected void Grilla_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        BE.GENERO g = gestor.Listar(int.Parse( Grilla.Rows[ int.Parse(e.CommandArgument.ToString())].Cells[2].Text    ));

        switch (e.CommandName)
        {
            case "btnSeleccionar":
                {
                    txtNombre.Text = g.Nombre;
                    hID.Value = g.Id.ToString();
                    break;
                }
            case "btnBorrar":
                {
                    gestor.Borrar(g);
                    txtNombre.Text = "";
                    hID.Value = "0";
                    Enlazar();
                    break;
                }
        }
    }
}