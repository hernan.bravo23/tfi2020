﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;

namespace BLL
{
    public class GESTOR_GENERO : GESTOR<BE.GENERO>
    {
        private DAL.MP_GENERO mp = new DAL.MP_GENERO();

        public override int Borrar(GENERO objeto)
        {
            return mp.Borrar(objeto);
        }

        public override int Grabar(GENERO objeto)
        {
            int resultado;
            if (objeto.Id == 0)
            {
                resultado = mp.Insertar(objeto);
            }
            else
            {
                resultado = mp.Editar(objeto);
            }


            return resultado;
        }

        public override List<GENERO> Listar()
        {
            return mp.Listar();
        }

        public override GENERO Listar(int id)
        {
            return (from BE.GENERO g in mp.Listar()
                    where g.Id == id
                    select g).First();
        }
    }
}
