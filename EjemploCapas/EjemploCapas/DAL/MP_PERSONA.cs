﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;


namespace DAL
{
   public class MP_PERSONA
    {
        private Acceso acceso = new Acceso();

        public List<BE.PERSONA> Listar()
        {
            DataTable tabla = acceso.Leer("PERSONA_LISTAR", null);
            List<BE.PERSONA> personas = new List<BE.PERSONA>();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.PERSONA persona = new BE.PERSONA();

                persona.Id = int.Parse(registro["id"].ToString());
                persona.Nombre = registro["nombre"].ToString();
                persona.Apellido = registro["apellido"].ToString();

                personas.Add(persona);
            }


            return personas;

        }

        public BE.PERSONA Listar(int ID)
        {
            DataTable tabla = acceso.Leer("PERSONA_LISTAR", null);
            BE.PERSONA persona = new BE.PERSONA();
            DataRow registro = tabla.Rows[0];

            persona.Id = int.Parse(registro["id"].ToString());
            persona.Nombre = registro["nombre"].ToString();
            persona.Apellido = registro["apellido"].ToString();

            return persona;
        }

        public int Insertar(BE.PERSONA persona)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(acceso.CrearParametro("@nom", persona.Nombre));
            parameters.Add(acceso.CrearParametro("@ape", persona.Apellido));

            return acceso.Escribir("PERSONA_INSERTAR", parameters);

        }

        public int Editar(BE.PERSONA persona)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(acceso.CrearParametro("@id", persona.Id));
            parameters.Add(acceso.CrearParametro("@nom", persona.Nombre));
            parameters.Add(acceso.CrearParametro("@ape", persona.Apellido));

            return acceso.Escribir("PERSONA_EDITAR", parameters);
        }

        public int Borrar(BE.PERSONA persona)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(acceso.CrearParametro("@id", persona.Id));
          
            return acceso.Escribir("PERSONA_BORRAR", parameters);
        }
    }
}
