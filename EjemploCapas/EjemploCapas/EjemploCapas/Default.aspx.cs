﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    BLL.PERSONA gestor = new BLL.PERSONA();


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            Enlazar();
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        hid.Value = "0";
        txtApe.Text = "";
        txtNom.Text = "";


    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrWhiteSpace(hid.Value))
        {
            hid.Value = "0";
        }

        BE.PERSONA per = new BE.PERSONA();
        per.Id = int.Parse(hid.Value);
        per.Nombre = txtNom.Text;
        per.Apellido = txtApe.Text;

        gestor.Grabar(per);
        Enlazar();
    }

    public void Enlazar()
    {
        GridView1.DataSource = gestor.Listar();
        GridView1.DataBind();
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int id = int.Parse(GridView1.Rows[int.Parse(e.CommandArgument.ToString())].Cells[2].Text);
        BE.PERSONA per = gestor.Listar(id);

        switch (e.CommandName)
        {
            case "Borrar":
                {
                    gestor.Borrar(per);
                    Enlazar();
                    break;
                }
            case "Seleccionar":
                {
             

                    hid.Value = per.Id.ToString();
                    txtNom.Text = per.Nombre;
                    txtApe.Text = per.Apellido;

                    break;
                }

        }
    }
}